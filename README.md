# DecaRounds

DecaRounuds is a traditional twin-stick shooter/bullet hell where each round lasts 10 seconds, and you have no time to rest between rounds.
Each round your own weapon changes, as well as a positive change is made to one of your stats. Additionally the round start marks the spawn of new enemies and objects around the arena.

Controls are traditional, WASD to move, mouse cursor to aim and left click to shoot. Unfortunately no mobile or controller support.

Goal is simply to last as many round as possible. If you can make it to double digits you've had a good run.

Game is quick to play, best of luck!

LDJam Link: https://ldjam.com/events/ludum-dare/51/decarounds
Itch.io Link: https://tharbakim.itch.io/decarounds

## Known Issues
On subsequent plays, the WASD keys can not be used to enter a player name.

## Post-Submission Changelog

* Added fix for attempting to fire weapon before it is initialized