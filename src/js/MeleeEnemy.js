export class MeleeEnemy extends Phaser.Physics.Arcade.Sprite {
    constructor(scene, position) {

        super(scene, position.x, position.y, 'meleeEnemy', Math.floor(Math.random() * 2))
        this.x = position.x
        this.y = position.y
        this.scene = scene
        this.xMomentum = 0
        this.yMomentum = 0
        this.maxMomentum = 175 + this.scene.gameState.round * 10
        this.body = new Phaser.Physics.Arcade.Body(scene.physics.world, this);
        scene.physics.world.enableBody(this, 0)
        this.scene = scene
        this.frameTime = 100
        this.frameIndex = 0
        this.health = 10
        this.nextManualFrame = 1
        this.meleeDamage = 1 * Math.floor((this.scene.gameState.round / 4))
        this.angle = 0
        this.followPlayer = false
        scene.add.existing(this);
        this.setCollideWorldBounds(true)
        this.projectileGroup = scene.enemyProjectileGroup
        this.wanderTimer = 0

    }

    hit(damage) {
        this.health -= damage
        this.scene.sound.play('enemyHit', {volume: window.soundVolume})
        if (this.health < 1) {
            this.destroy()
        }
        this.followPlayer = true
    }

    getMeleeDamage() {
        return this.meleeDamage
    }

    spin(speed = 20) {
        this.angle = (this.angle + speed) % 360
    }

    wander() {
        this.wanderTimer = new Date()
        const xMomentum = (Math.random() * 2000) - 1000
        const yMomentum = (Math.random() * 2000) - 1000
        const totalMomentum = Math.abs(xMomentum) + Math.abs(yMomentum)
        this.xMomentum = (xMomentum / totalMomentum * this.maxMomentum)
        this.yMomentum = (yMomentum / totalMomentum * this.maxMomentum)
    }

    update(player, game) {
        if(this.followPlayer === false && Phaser.Math.Distance.Between(player.x, player.y, this.x, this.y) < 400) {
            this.followPlayer = true
        }
        if (this.followPlayer === true) {
            const xMomentum = player.x - this.x
            const yMomentum = player.y - this.y
            const totalMomentum = Math.abs(xMomentum) + Math.abs(yMomentum)
            this.xMomentum = (xMomentum/totalMomentum * this.maxMomentum)
            this.yMomentum = (yMomentum/totalMomentum * this.maxMomentum)
            this.spin(20)
        } else {
            if ((this.wanderTimer + 3000) < new Date()) {
                this.wander()
            }
            this.spin(12)
        }

        this.setVelocityX(this.xMomentum)
        this.setVelocityY(this.yMomentum)
    }
}
