export class GameMap {
    static TILE_WIDTH = 32
    static TILE_HEIGHT = 32

    constructor() {
        this.direction = 1
        this.width = 7
        this.defaultTile = { x: 0, y: 0, textureType: 'floor', texture: 0 }
        this.map = [[{ x: 0, y: 0, textureType: 'floor', texture: 6 }]]

        this.generateRoom({ x: 0, y: 0 }, 35, 35)
        this.addBorders()
        this.smartTexture(this.getTilesByType('wall'))
        this.setCorners()
    }

    generateRoom(start, width, height) {
        if (height > 0) {
            while (this.map.length < (start.y + height)) {
                // We can get away with an empty array here, it'll be populated by the width loop
                this.map.push([])
            }
        }

        if (height < 0) {
            let i = height
            let j = 0
            while (start.y + i < 0) {
                // We can get away with an empty array here, it'll be populated by the width loop
                this.map.unshift([])
                i++
                j++
            }
            start.y += j
        }

        // When width < 0, any added rows aren't padded properly. To work around, pad every row after every room is generated.
        let maxLength = 0
        this.map.forEach(row => {
            if (row.length > maxLength) {
                maxLength = row.length
            }
        })

        this.map.forEach(row => {
            while (row.length < maxLength) {
                row.push(Object.assign({}, this.defaultTile))
            }
        })

        if (width < 0) {
            const startWidth = this.map[0].length
            let j = 0
            this.map.forEach(row => {
                let i = width
                while (start.x + i < 0) {
                    row.unshift(Object.assign({}, this.defaultTile))
                    i++
                }
            })
            const endWidth = this.map[0].length
            start.x += endWidth - startWidth
        }

        if (width > 0) {
            this.map.forEach(row => {
                while (row.length < start.x + width + 1) {
                    row.push(Object.assign({}, this.defaultTile))
                }
            })
        }

        if (height > 0) {
            for (let i = 0; i < height; i++) {
                if (width > 0) {
                    for (let j = 0; j < width; j++) {
                        this.map[start.y + i][start.x + j].textureType = 'floor'
                        this.map[start.y + i][start.x + j].texture = this.getFloorTexture()
                    }
                } else if (width < 0) {
                    for (let j = 0; j > width; j--) {
                        this.map[start.y + i][start.x + j].textureType = 'floor'
                        this.map[start.y + i][start.x + j].texture = this.getFloorTexture()
                    }
                }
            }
        } else if (height < 0) {
            for (let i = 0; i > height; i--) {
                if (width > 0) {
                    for (let j = 0; j < width; j++) {
                        this.map[start.y + i][start.x + j].textureType = 'floor'
                        this.map[start.y + i][start.x + j].texture = this.getFloorTexture()
                    }
                } else if (width < 0) {
                    for (let j = 0; j > width; j--) {
                        this.map[start.y + i][start.x + j].textureType = 'floor'
                        this.map[start.y + i][start.x + j].texture = this.getFloorTexture()
                    }

                }
            }
        }
        /*let edgeTiles = []
        const roomWidth = Math.abs(width)
        const roomHeight = Math.abs(height)
        for (let i = 0; i < roomWidth; i++) {
            edgeTiles.push({ x: start.x + i, y: start.y })
            edgeTiles.push({ x: start.x + i, y: start.y + roomHeight })
        }
        for (let i = 0; i < roomHeight; i++) {
            edgeTiles.push({ x: start.x, y: start.y + i }
            edgeTiles.push({ x: start.x + roomWidth, y: start.y + i })
        }
        return edgeTiles[Math.floor(Math.random() * edgeTiles.length)]*/
        return { x: start.x + Math.floor(Math.random() * width), y: start.y + Math.floor(Math.random() * height) }

    }

    smartTexture(textures) {
        if (Array.isArray(textures)) {
            textures.forEach(texture => {
                this._smartTexture(texture)
            })
        } else {
            this._smartTexture(textures)
        }
    }

    _smartTexture(texture) {
        let neighbours = 0
        let neighbourIds = []
        let voidTiles = 0
        try {
            if (this.map[texture.y + 1][texture.x].textureType === 'wall') {
                neighbours++
                neighbourIds.push(0)
            }
        } catch (e) { voidTiles++ }
        try {
            if (this.map[texture.y - 1][texture.x].textureType === 'wall') {
                neighbours++
                neighbourIds.push(2)

            }
        } catch (e) { voidTiles++ }
        try {
            if (this.map[texture.y][texture.x + 1].textureType === 'wall') {
                neighbours++
                neighbourIds.push(1)
            }
        } catch (e) { voidTiles++ }
        try {
            if (this.map[texture.y][texture.x - 1].textureType === 'wall') {
                neighbours++
                neighbourIds.push(3)
            }
        } catch (e) { voidTiles++ }

        if (voidTiles > 0 && neighbours + voidTiles === 4) {
            this.map[texture.y][texture.x].invisible = true
            return
        }

        if (neighbours === 0) {
            this.map[texture.y][texture.x].physics = true
            //Island
            this.map[texture.y][texture.x].texture = 5
        }

        if (neighbours === 1) {
            this.map[texture.y][texture.x].physics = true

            //Edge piece
            this.map[texture.y][texture.x].texture = 4
            if (neighbourIds.includes(0)) {
                this.map[texture.y][texture.x].angle = 270
            } else if (neighbourIds.includes(1)) {
                this.map[texture.y][texture.x].angle = 180
            } else if (neighbourIds.includes(2)) {
                this.map[texture.y][texture.x].angle = 90
            } else if (neighbourIds.includes(3)) {
                this.map[texture.y][texture.x].angle = 0
            }
        } else if (neighbours === 2) {
            this.map[texture.y][texture.x].physics = true
            //Straight border
            if (Math.abs(neighbourIds[0] - neighbourIds[1]) === 2) {
                texture.texture = 3
                if ((neighbourIds[0] % 2) === 0) {
                    //Vertical Wall
                    if (this.map[texture.y][texture.x + 1] !== undefined &&
                        this.map[texture.y][texture.x + 1].textureType === 'floor') {
                        //Left Wall
                        texture.angle = 270
                    } else {
                        texture.angle = 90
                    }
                } else {
                    //Horizontal Wall
                    if (this.map[texture.y + 1] !== undefined &&
                        this.map[texture.y + 1][texture.x].textureType === 'floor') {
                        //Top Wall
                        texture.angle = 0
                    } else {
                        texture.angle = 180
                    }
                }
            }
        }
    }

    setCorners() {
        this.map[0][0].angle = 180
        this.map[0][this.map[0].length - 1].angle = 270
        this.map[this.map.length - 1][0].angle = 90
        this.map[this.map.length - 1][this.map[0].length - 1].angle = 0
    }

    getFloorTexture() {
        //0 is basic
        //1-4 have features
        //5-8 have subtle colour changes

        const weights = {
            0: 100,
            1: 100,
            2: 100,
            3: 100,
            4: 100,
            5: 100,
            6: 100,
            7: 100,
            8: 100
        }
        const weightTotal = Object.values(weights).reduce((partialSum, a) => partialSum + a, 0)

        const random = Math.floor(Math.random() * weightTotal + 1)
        let offset = 0
        for (const [key, value] of Object.entries(weights)) {
            offset += value
            if (offset > random) {
                return key
            }
        }
    }

    addBorders() {
        this.map.push([])
        this.map.unshift([])
        let maxLength = 0
        this.map.forEach(row => {
            if (row.length > maxLength) {
                maxLength = row.length
            }
        })
        let y = 0
        this.map.forEach(row => {
            while (row.length < maxLength) {
                row.push(Object.assign({}, { x: row.length, y: y, textureType: 'wall', texture: 6 }))
            }
            row.unshift(Object.assign({}, { x: 0, y: 0, textureType: 'wall', texture: 6 }))
            row.push(Object.assign({}, { x: 0, y: 0, textureType: 'wall', texture: 6 }))
            y++
        })
    }

    getTilesByType(type, sprite = null) {
        let wallTiles = []
        for (let y = 0; y < this.map.length; y++) {
            for (let x = 0; x < this.map[0].length; x++) {
                this.map[y][x].x = x
                this.map[y][x].y = y
                if (this.map[y][x].textureType === type) {
                    if (sprite === null || this.map[y][x].texture == sprite) {
                        wallTiles.push(this.map[y][x])
                    }
                }
            }
        }
        return wallTiles
    }

    getHeight() {
        return this.map.length * GameMap.TILE_HEIGHT
    }

    getWidth() {
        return this.map[0].length * GameMap.TILE_WIDTH
    }

    getStart() {
        return { x: this.getWidth() / 2, y: this.getHeight() / 2 }
    }


    draw(game) {

        this.tileMap = []
        let rowId = 0
        this.map.forEach(row => {
            let columnId = 0
            row.forEach(column => {
                if (!(Array.isArray(this.tileMap[rowId]))) {
                    this.tileMap[rowId] = []
                }
                this.tileMap[rowId][columnId] = game.add.sprite((columnId * GameMap.TILE_WIDTH) + (GameMap.TILE_WIDTH / 2), (rowId * GameMap.TILE_HEIGHT) + (GameMap.TILE_HEIGHT / 2), this.map[rowId][columnId].textureType, this.map[rowId][columnId].texture)
                if (this.map[rowId][columnId].physics === true) {
                    game.physics.world.enable(this.tileMap[rowId][columnId])
                    this.tileMap[rowId][columnId].body.setImmovable(true)
                    this.tileMap[rowId][columnId].body.setMass(1000)
                }
                if (this.map[rowId][columnId].hasOwnProperty('angle')) {
                    this.tileMap[rowId][columnId].angle = this.map[rowId][columnId].angle
                }
                if (this.map[rowId][columnId].textureType = 'wall') {
                    game.wallGroup.add(this.tileMap[rowId][columnId])
                }
                columnId++
            })
            rowId++
        })
    }
}