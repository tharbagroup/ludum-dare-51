import { GameSettings } from './GameSettings.js'
import { GameState } from './GameState.js'

export let SceneShop = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function () {
        Phaser.Scene.call(this, { "key": "SceneShop" })
        let resize = () => {
            try {
                if (this.sys.game.scene.isActive(this.scene.key)) {
                   this.scene.restart()
                }
            } catch (e) { }
        }


        window.addEventListener("resize", resize.bind(this), false);
    },
    init: function (data) {
        if (!(data.gameSettings instanceof GameSettings)) {
            throw 'GameSettings undefined.'
        } else {
            this.gameSettings = data.gameSettings
        }
        if (!(data.gameState instanceof GameState)) {
            throw 'GameState undefined.'
        } else {
            this.gameState = data.gameState
        }
    },
    preload: function () {
        this.load.spritesheet('uiBorder', 'images/items.png', {
            frameWidth: 32,
            frameHeight: 32
        })
    },
    create: function () {
        const background = this.add.graphics()
        background.fillStyle(0xCCCCCC, 0.7)

        background.fillRoundedRect(100, 100, this.sys.game.canvas.width - 200, this.sys.game.canvas.height - 200, 32)
        background.setDepth(20)
        background.setScrollFactor(0)

        let listStartHeight = 116



        try {

            this.gameState.availableItems.forEach(item => {
                if (item.price != 0) {
            const itemBackground = this.add.graphics()
            itemBackground.fillStyle(0xFFFFFF, 0.9)

            itemBackground.fillRoundedRect(116, listStartHeight, this.sys.game.canvas.width - 232, 64, 4)
            itemBackground.setDepth(21)
            itemBackground.setScrollFactor(0)
            let itemInset = 132
            const itemIcon = this.add.sprite(itemInset, listStartHeight + 32, 'items', item.frame)
                .setDepth(22)
                .setOrigin(0.5, 0.5)
                .setScrollFactor(0, 0)

            itemInset += 32
            const itemName = this.add.text(itemInset, listStartHeight + 32, `${item.name}`, {
                fontSize: 32,
                color: "#000000",
                fontStyle: "bold",
            }).setDepth(22)
                .setOrigin(0, 0.5)
                .setScrollFactor(0, 0)

            itemInset += itemName.displayWidth + 16

            if (itemInset < this.sys.game.canvas.width - 300) {
                const itemDescription = this.add.text(itemInset, listStartHeight + 32, `${item.description}`, {
                    fontSize: 14,
                    color: "#777777",
                    fontStyle: "bold",
                }).setDepth(22)
                    .setOrigin(0, 0.5)
                    .setScrollFactor(0, 0)

                itemInset += itemDescription.displayWidth + 16
            }
                let itemColor = '#ff8888'
            if (item.price < this.gameState.money) {
                itemColor = '#88ff88'
            }
            const itemBuy = this.add.text(this.sys.game.canvas.width - 132, listStartHeight + 32, `${item.price}`, {
                fontSize: 32,
                color: '#777777',
                fontStyle: "bold",
                backgroundColor: itemColor,
                padding: { left: 37, right: 5, y: 5 }
            }).setDepth(22)
                .setOrigin(1, 0.5)
                .setScrollFactor(0, 0)
            if (item.price < this.gameState.money) {
                itemBuy.setInteractive({ useHandCursor: true })
                    .on('pointerdown', () => {
                        this.gameState.buyItem(item.name)
                        this.gameState.updateUI = true
                        this.scene.restart()
                    })

            }

            const itemBuyIcon = this.add.sprite(this.game.canvas.width - 132 - (itemBuy.displayWidth), listStartHeight + 32, 'items', '0')
                .setDepth(23)
                .setOrigin(0, 0.5)
                .setScrollFactor(0, 0)

            


            listStartHeight += 72
            if (listStartHeight + 64 > (this.sys.game.canvas.height - 250)) {
                throw RangeError
            }
        }
        })
    
    } catch (e) { }
let saveButton = this.add.text(
    this.sys.game.canvas.width / 2,
    this.sys.game.canvas.height - 150,
    "Return to Game",
    {
        fontSize: 32,
        color: "#FFFFFF",
        fontStyle: "bold",
        backgroundColor: "#88ff88",
        padding: { x: 10, y: 10 },

    }
)
    .setOrigin(0.5)
    .setInteractive({ useHandCursor: true })
    .setDepth(21)
    .on('pointerdown', () => {
        this.scene.stop('SceneShop')
        this.gameState.paused = false
    })
    },
update: function () {

}
})