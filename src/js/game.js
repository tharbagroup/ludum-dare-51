import { SceneStartGame } from './SceneStartGame.js'
import { SceneGameSettings } from './SceneGameSettings.js' 
import { SceneSplash } from './SceneSplash.js'
import { SceneDay } from './SceneDay.js'
import { SceneBackground } from './SceneBackground.js'
import { SceneUI } from './SceneUI.js'
import { SceneShop } from './SceneShop.js'
import { SceneGameOver } from './SceneGameOver.js'
import { Timer } from './Timer.js'

const phaserSettings = {
    type: Phaser.AUTO,
    parent: "game",
    width: window.innerWidth,
    height: window.innerHeight,
    backgroundColor: "#000000",
    scene: [ SceneSplash, SceneStartGame, SceneGameSettings, SceneDay, SceneBackground, SceneUI, SceneShop, SceneGameOver ],
    dom: {
        createContainer: true
    },
    physics: {
        default: 'arcade'
    },
    pixelArt: true
}

window.soundVolume = 1

const game = new Phaser.Game(phaserSettings)
window.addEventListener('resize', () => {
    game.scale.resize(window.innerWidth, window.innerHeight)
})
/*
const backgroundSound = new Audio('./sounds/background.wav')
backgroundSound.addEventListener('ended', function() {
    this.currentTime = 0
    this.play()
}, false)
window.addEventListener('focus', () => {
    backgroundSound.play()
})


window.addEventListener('blur', () => {
    backgroundSound.pause()
})

window.addEventListener('sound-off', () => {
    backgroundSound.volume = 0
})

window.addEventListener('sound-on', () => {
    backgroundSound.volume = 1
})
*/
window.timer = new Timer()
