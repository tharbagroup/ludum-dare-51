export class Timer
{
    constructor()
    {
        this.active = false
        this.lastRun = new Date()
        this.remainingTime = 10000
        this.tick = new Event('tick')
        this.pause = new Event('pause')
        this.resume = new Event('resume')

        window.addEventListener('pause', (event) => {
            this.pauseTimer()
        })

        window.addEventListener('resume', (event) => {
            this.resumeTimer()
        })
    }

    setActive = () => {
        if (this.active !== true) {

            this.active = true
            this.timer = setInterval(this.sendEvent, 10000)
        }
    }

    setInactive = () => {
        if (this.active !== false) {
            this.active = false
            clearInterval(this.timer)
        }
    }

    sendEvent = () => {
        this.lastRun = new Date()
        window.dispatchEvent(this.tick)
        this.setActive()
    }

    pauseTimer = () => {
        this.remainingTime = new Date() - this.lastRun
        clearTimeout(this.timeout)
        this.setInactive()
    }

    resumeTimer = () => {
        this.timeout = setTimeout(this.sendEvent, this.remainingTime)
    }
}