import { GameSettings } from './GameSettings.js'
import { GameState } from './GameState.js'

export let SceneGameOver = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function () {
        Phaser.Scene.call(this, { "key": "SceneGameOver" })
                let resize = () => {
            if (this.sys.game.scene.isActive(this.scene.key)) {
               this.scene.restart()
            }
        }

        window.addEventListener("resize", resize.bind(this), false);
    },
    init: function (data) {
        if (!(data.gameSettings instanceof GameSettings)) {
            throw 'GameSettings undefined.'
        } else {
            this.gameSettings = data.gameSettings
        }
        if (!(data.gameState instanceof GameState)) {
            throw 'GameState undefined.'
        } else {
            this.gameState = data.gameState
        }
    },
    preload: () => {
    },
    create: function () {
        this.scene.stop('SceneBackground')
        this.scene.stop('SceneUI')
        let menuText = this.add.text(
            this.sys.game.canvas.width / 2,
            this.sys.game.canvas.height / 3,
            "Game Over",
            {
                fontSize:64,
                color: "#55FF55",
                fontStyle: "bold"
            }
        ).setOrigin(0.5)

        let subMenuText = this.add.text(
            this.sys.game.canvas.width / 2,
            this.sys.game.canvas.height / 3 + menuText.height,
            `Died on round ${this.gameState.round}`,
            {
                fontSize:16,
                color: "#55FF55",
                fontStyle: "bold"
            }
        ).setOrigin(0.5)


        let startButton = this.add.text(
            this.sys.game.canvas.width / 2,
            this.sys.game.canvas.height / 1.5,
            "Start",
            {
                fontSize: 32,
                color: "#FFFFFF",
                fontStyle: "bold",
                backgroundColor: "#8888ff",
                padding: { x: 10, y: 10 },

            }
        )
            .setOrigin(0.5)
            .setInteractive({ useHandCursor: true })
            .on('pointerdown', () => {
                this.scene.start("SceneSplash",{gameSettings: this.gameSettings, gameState:  new GameState()})
            })
    },
    startGame: function () {
    },
    update: function () {

    }
})