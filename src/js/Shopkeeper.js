export class Shopkeeper extends Phaser.Physics.Arcade.Sprite {
    constructor(scene, x, y) {

        super(scene, x, y, 'shopkeeper', 0);
        this.xMomentum = 0
        this.yMomentum = 0
        this.visible = true
        this.body = new Phaser.Physics.Arcade.Body(scene.physics.world, this);
        scene.physics.world.enableBody(this, 0)
        this.depth = 5
        this.x = (x * 32) + 16
        this.y = (y * 32) + 16
        this.activate = () => {
            scene.gameState.paused = true
            scene.scene.launch('SceneShop', { gameState: scene.gameState, gameSettings: scene.gameSettings })
            scene.scene.bringToTop('SceneShop')

        }
        scene.add.existing(this);
    }
}
