export let SceneGameSettings = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function () {
        Phaser.Scene.call(this, { "key": "SceneGameSettings" })
                let resize = () => {
            if (this.sys.game.scene.isActive(this.scene.key)) {
               this.scene.restart()
            }
        }

        window.addEventListener("resize", resize.bind(this), false);
    },
    init: function (gameSettings) {
        this.gameSettings = gameSettings
    },
    preload: () => { },
    create: function () {

        const activeBackground = "#dd6666"
        const inactiveBackground = "#ff8888"
        const soundOffColor = (window.soundVolume === 1) ? inactiveBackground : activeBackground
        const soundOnColor = (window.soundVolume === 0) ? inactiveBackground : activeBackground

        
        let soundOffButton = this.add.text(
            this.sys.game.canvas.width / 2 - 120,
            this.sys.game.canvas.height / 2,
            "Sound Off",
            {
                fontSize: 32,
                color: "#FFFFFF",
                fontStyle: "bold",
                backgroundColor: soundOffColor,
                padding: { x: 20, y: 20 },

            }
        )
            .setOrigin(0.5)
            .setInteractive({ useHandCursor: true })
            .on('pointerdown', () => {
                window.soundVolume = 0
                this.scene.restart()
            })

        let soundOnButton = this.add.text(
            this.sys.game.canvas.width / 2 + 120,
            this.sys.game.canvas.height / 2,
            "Sound On",
            {
                fontSize: 32,
                color: "#FFFFFF",
                fontStyle: "bold",
                backgroundColor: soundOnColor,
                padding: { x: 20, y: 20 },

            }
        )
            .setOrigin(0.5)
            .setInteractive({ useHandCursor: true })
            .on('pointerdown', () => {
                window.soundVolume = 1
                this.scene.restart()
            })



        let settingsButton = this.add.text(
            this.sys.game.canvas.width / 2,
            this.sys.game.canvas.height / 1.5,
            "Back",
            {
                fontSize: 32,
                color: "#FFFFFF",
                fontStyle: "bold",
                backgroundColor: "#8888ff",
                padding: { x: 10, y: 10 },

            }
        )
            .setOrigin(0.5)
            .setInteractive({ useHandCursor: true })
            .on('pointerdown', () => {
                this.scene.start("SceneSplash")
            })


    },
    update: () => { }
})