import { StatusMessage } from './StatusMessage.js'

export class Player extends Phaser.Physics.Arcade.Sprite {
    constructor(scene, x, y, weapon) {

        super(scene, x, y, 'player', 0);
        this.scene = scene
        this.xMomentum = 0
        this.yMomentum = 0
        this.body = new Phaser.Physics.Arcade.Body(scene.physics.world, this);
        scene.physics.world.enableBody(this, 0)
        this.scene = scene
        this.weapon = weapon
        this.frameTime = 100
        this.frameIndex = 0
        this.nextManualFrame = 1
        this.gameState = scene.gameState
        this.projectileGroup = scene.projectileGroup
        this.invulnerable = false;
        this.invulnerableTimer = 0
        this.bulletMass = 1
        this.damageMultiplier = 1

        scene.add.existing(this);
        this.setCollideWorldBounds(true)

        this.gameState.weaponName = weapon.name
    }

    setXMomentum(change) {
        if (Math.abs(this.xMomentum + change) > this.scene.gameState.maxSpeed) {
            return Math.sign(change) * this.scene.gameState.maxSpeed
        } else {
            return this.xMomentum + change
        }
    }

    calculateNextFrame() {

        const hold = this.frameIndex
        this.frameIndex = this.nextManualFrame
        this.setTexture('player', this.frameIndex)

        if (hold === 1) {
            this.nextManualFrame = 2
        } else if (hold === 2) {
            this.nextManualFrame = 1
        }
        else {
            this.nextManualFrame = 0
        }
    }

    runTickMutations() {
        switch (parseInt(this.randomRoll())) {
            case 0:
                this.scene.gameState.maxSpeed += 25
                this.scene.gameState.accelerationRate += 10
                this.statusNotice('Maximum Speed Increased')
                break;
            case 1:
                this.scene.gameState.health += 2
                this.scene.gameState.updateUI = true
                this.statusNotice('Gained 2 Health')
                break;
            case 2:
                this.scene.gameState.damageMultiplier += 0.25
                this.statusNotice('Damage Multiplier Increased')
                break;
            case 3:
                this.scene.gameState.health += 5
                this.scene.gameState.updateUI = true
                this.statusNotice('Gained 5 Health')
                break;
            case 4:
                this.scene.gameState.meleeDamage++
                this.statusNotice('Melee Damage Increased')
                break;
            case 5:
                this.scene.gameState.invulnerableDuration += 100
                this.statusNotice('Invulnerable Time Increased')
                break;
            case 6:
                this.scene.gameState.changeFireRate(0.02)
                this.statusNotice('Fire Rate Increased')
                break
            case 7:
                this.scene.gameState.projectileSpeed += 10
                this.statusNotice('Projectile Speed Increased')
                break
        }
    }

    statusNotice(message) {
        this.scene.statusMessages.push(new StatusMessage(this.scene, this.x, this.y, message))
    }

    setInvulnerable() {
        this.invulnerable = true
        this.invulnerableTimer = new Date()
    }

    randomRoll() {
        const weights = {
            0: 100,
            1: 100,
            2: 100,
            3: 50,
            4: 50,
            5: 25,
            6: 25,
        }
        const weightTotal = Object.values(weights).reduce((partialSum, a) => partialSum + a, 0)

        const random = Math.floor(Math.random() * weightTotal + 1)
        let offset = 0
        for (const [key, value] of Object.entries(weights)) {
            offset += value
            if (offset > random) {
                return key
            }
        }
    }



    hit(damage) {
        if (this.invulnerable === false) {
            this.scene.gameState.health -= parseInt(damage)
            if (this.scene.gameState.health < 1) {
                this.scene.gameOver()
            }
            this.scene.sound.play('playerHit', {volume: window.soundVolume})
            this.setInvulnerable()
            this.scene.gameState.updateUI = true
        }
    }

    getMeleeDamage() {
        return this.scene.gameState.meleeDamage
    }

    setWeapon(weapon) {
        this.weapon = weapon
        this.weapon.setDamageMultiplier(this.scene.gameState.damageMultiplier)
    }

    setYMomentum(change) {
        if (Math.abs(this.yMomentum + change) > this.scene.gameState.maxSpeed) {
            return Math.sign(change) * this.scene.gameState.maxSpeed
        } else {
            return this.yMomentum + change
        }
    }

    moveLeft() {
        this.xMomentum = this.setXMomentum(-this.scene.gameState.accelerationRate)
    }

    moveRight() {
        this.xMomentum = this.setXMomentum(this.scene.gameState.accelerationRate)
    }

    moveUp() {
        this.yMomentum = this.setYMomentum(-this.scene.gameState.accelerationRate)
    }

    moveDown() {
        this.yMomentum = this.setYMomentum(this.scene.gameState.accelerationRate)
    }

    aimAtCursor(x, y) {
        this.angle = Phaser.Math.RadToDeg(Phaser.Math.Angle.Between(window.innerWidth / 2, window.innerHeight / 2, x, y))

    }

    stationary() {
        if (this.xMomentum > 0) {
            this.xMomentum = Math.floor(this.xMomentum / this.scene.gameState.frictionRate)
        } else if (this.xMomentum < 0) {
            this.xMomentum = Math.ceil(this.xMomentum / this.scene.gameState.frictionRate)
        }



        if (this.yMomentum > 0) {
            this.yMomentum = Math.floor(this.yMomentum / this.scene.gameState.frictionRate)
        } else if (this.yMomentum < 0) {
            this.yMomentum = Math.ceil(this.yMomentum / this.scene.gameState.frictionRate)
        }
    }

    fire(game, mouseX, mouseY) {
        try {
            this.weapon.fireAtMouse(game, mouseX, mouseY, this, this.xMomentum, this.yMomentum)
        } catch {}
    }



    update() {
        this.frameTime -= ((Math.abs(this.xMomentum) + Math.abs(this.yMomentum)) / 2)
        if (this.frameTime < 0) {
            this.frameTime = 800
            this.calculateNextFrame()
        }
        this.setVelocityX(this.xMomentum)
        this.setVelocityY(this.yMomentum)
        this.stationary()
        if (this.invulnerable === true) {
            if ((new Date() - this.scene.gameState.invulnerableDuration) > this.invulnerableTimer) {
                this.invulnerable = false
            }
        }
    }
}
