import { GameSettings } from './GameSettings.js'
import { GameState } from './GameState.js'
import { GameMap } from './GameMap.js'
import { Player } from './Player.js'
import { Enemy } from './Enemy.js'
import { MeleeEnemy } from './MeleeEnemy.js'
import { PlayerWeaponRandomizer } from './PlayerWeaponRandomizer.js'
import { Breakable } from './Breakable.js'
import { SceneBackground } from './SceneBackground.js'

export let SceneDay = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function () {
        Phaser.Scene.call(this, { "key": "SceneDay" })
        let resize = () => {
            if (this.sys.game.scene.isActive(this.scene.key)) {
                //              //this.scene.restart()
            }
        }

        window.addEventListener("resize", resize.bind(this), false);
    },
    init: function (data) {
        if (!(data.gameSettings instanceof GameSettings)) {
            throw 'GameSettings undefined.'
        } else {
            this.gameSettings = data.gameSettings
        }
        if (!(data.gameState instanceof GameState)) {
            throw 'GameState undefined.'
        } else {
            this.gameState = data.gameState
        }

        this.cursors = {
            'left': this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A),
            'up': this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W),
            'right': this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D),
            'down': this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S),
        }

    },
    preload: function () {
        this.load.spritesheet('player', 'images/player.png', {
            frameWidth: 32,
            frameHeight: 32
        })
        this.load.spritesheet('shopkeeper', 'images/shopkeeper.png', {
            frameWidth: 32,
            frameHeight: 32
        })
        this.load.spritesheet('meleeEnemy', 'images/meleeEnemy.png', {
            frameWidth: 32,
            frameHeight: 32
        })
        this.load.image('door', 'images/door.png')
        this.load.spritesheet('tiles', 'images/tiles.png', {
            frameWidth: 32,
            frameHeight: 32
        })
        this.load.spritesheet('floor', 'images/floor.png', {
            frameWidth: 32,
            frameHeight: 32
        })
        this.load.spritesheet('wall', 'images/wall.png', {
            frameWidth: 32,
            frameHeight: 32
        })
        this.load.spritesheet('breakable', 'images/breakable.png', {
            frameWidth: 32,
            frameHeight: 32
        })
        this.load.spritesheet('projectile', 'images/projectile.png', {
            frameWidth: 4,
            frameHeight: 4
        })

        this.load.spritesheet('large-projectile', 'images/large-projectile.png', {
            frameWidth: 16,
            frameHeight: 16
        })
        this.load.spritesheet('laser-projectile', 'images/laser-projectile.png', {
            frameWidth: 12,
            frameHeight: 4
        })
        this.load.spritesheet('wave-projectile', 'images/wave-projectile.png', {
            frameWidth: 4,
            frameHeight: 16
        })

        this.load.audio('boxBreak', 'sounds/break.wav')
        this.load.audio('enemyHit', 'sounds/enemy-hit-1.wav')
        this.load.audio('explosion', 'sounds/explosion.wav')
        this.load.audio('laserShoot-1', 'sounds/laserShoot-1.wav')
        this.load.audio('laserShoot-2', 'sounds/laserShoot-2.wav')
        this.load.audio('laserShoot-3', 'sounds/laserShoot-3.wav')
        this.load.audio('playerHit', 'sounds/player-hit.wav')
        this.load.audio('powerUp-1', 'sounds/powerUp-1.wav')
        this.load.audio('genericShoot-1', 'sounds/genericShoot-1.wav')
        this.load.audio('waveShoot-1', 'sounds/waveShoot-1.wav')
        this.load.audio('background', 'sounds/background.wav')

        //Is this needed?
        this.textures.get('floor').setFilter(1)
    },
    create: function () {

        this.cameras.main.setZoom(3)
        this.gameState.gameMap = new GameMap(this.gameState.day)



        this.projectileGroup = this.add.group()
        this.enemyProjectileGroup = this.add.group()
        this.wallGroup = this.add.group()
        this.enemyGroup = this.add.group()
        this.breakableGroup = this.add.group()
        this.statusMessages = []

        this.playerWeaponRandomizer = new PlayerWeaponRandomizer()
        //Player before map, map creates the collider
        //Initialize at 0, 0 so we can place in a valid square at the start of the game
        this.player = new Player(this, 0, 0, this.playerWeaponRandomizer.getRandomWeapon(this.gameState.fireRate, this.gameState.projectileSpeed))
        this.player.depth = 10

        this.gameState.gameMap.draw(this)
        const startPosition = this.gameState.gameMap.getStart()
        this.player.x = startPosition.x
        this.player.y = startPosition.y
        const UI = this.scene.get('SceneUI')
        this.scene.launch('SceneBackground')
        this.scene.sendToBack('SceneBackground')
        this.scene.launch('SceneUI', { gameState: this.gameState, gameSettings: this.gameSettings })
        this.scene.bringToTop('SceneUI')



        const maxWidth = (this.gameState.gameMap.getWidth() * this.cameras.main.zoom + (this.cameras.main.displayWidth / 2))
        const minWidth = -(this.cameras.main.displayWidth / 2)
        const maxHeight = (this.gameState.gameMap.getHeight() * this.cameras.main.zoom + (this.cameras.main.displayHeight / 2))
        const minHeight = -(this.cameras.main.displayHeight / 2)

        this.physics.world.setBounds(0, 0, this.gameState.gameMap.getWidth(), this.gameState.gameMap.getHeight())

        const playableZone = { xMin: 32, yMin: 32, xMax: this.gameState.gameMap.getWidth() - 64, yMax: this.gameState.gameMap.getHeight() - 64 }

        const getRandomPosition = () => {
            return { x: Math.floor(Math.random() * (playableZone.xMax - playableZone.xMin + 1)) + playableZone.xMin, y: Math.floor(Math.random() * (playableZone.yMax - playableZone.yMin + 1)) + playableZone.yMin }
        }

        this.cameras.main.setBounds(minWidth, minHeight, maxWidth, maxHeight)
        this.cameras.main.fadeIn(1000, 0, 0, 0)
        this.cameras.main.startFollow(this.player)
        this.gameState.levelStart = performance.now()
        window.timer.setActive()
        this.backgroundMusic = this.sound.add('background')
        this.backgroundMusic.play({loop: true, volume: window.soundVolume })
        this.gameTick = () => {
        for (let i = 0; i < Math.ceil(Math.random() * (this.gameState.round / 3) + 2); i++) {
            this.enemyGroup.add(new Enemy(this, getRandomPosition(), this.playerWeaponRandomizer.getRandomWeapon(this.gameState.fireRate, this.gameState.projectileSpeed)))
        }
        for (let i = 0; i < Math.ceil(Math.random() * (this.gameState.round / 3) + 2); i++) {
            this.enemyGroup.add(new MeleeEnemy(this, getRandomPosition()))
        }
        
        for (let i = 0; i < Math.ceil(Math.random() * (this.gameState.round / 6) + 1); i++) {
            this.breakableGroup.add(new Breakable(this, getRandomPosition(), this.playerWeaponRandomizer.getRandomWeapon(this.gameState.fireRate, this.gameState.projectileSpeed)))
        }

        this.player.runTickMutations()
        this.player.setWeapon(this.playerWeaponRandomizer.getRandomWeapon(this.gameState.fireRate, this.gameState.projectileSpeed))
        this.gameState.round++
    }

        this.gameTickHandler = this.gameTick.bind(this)
        window.addEventListener('tick', this.gameTickHandler)

        for (let i = 0; i < 3; i++) {
            this.enemyGroup.add(new Enemy(this, getRandomPosition(), this.playerWeaponRandomizer.getRandomWeapon(this.gameState.fireRate, this.gameState.projectileSpeed)))
            this.enemyGroup.add(new MeleeEnemy(this, getRandomPosition(), this.playerWeaponRandomizer.getRandomWeapon()))
            this.breakableGroup.add(new Breakable(this, getRandomPosition(), this.playerWeaponRandomizer.getRandomWeapon()))
        }
    },
    startGame: function () {
    },
    update: function () {
        if (this.gameState.paused != true) {
            if (this.input.keyboard.checkDown(this.cursors.left)) {
                this.player.moveLeft()
            }
            else if (this.input.keyboard.checkDown(this.cursors.right)) {
                this.player.moveRight()
            }

            if (this.input.keyboard.checkDown(this.cursors.up)) {
                this.player.moveUp()
            }
            else if (this.input.keyboard.checkDown(this.cursors.down)) {
                this.player.moveDown()
            }

            //This breaks support for touch devices
            this.player.aimAtCursor(this.input.mousePointer.x, this.input.mousePointer.y)

            if (this.input.mousePointer.leftButtonDown()) {
                this.player.fire(this, this.input.mousePointer.x, this.input.mousePointer.y)
            }
        }
        this.enemyGroup
        this.physics.collide(this.projectileGroup, this.wallGroup, this.wallCollisionRouter);
        this.physics.collide(this.enemyProjectileGroup, this.wallGroup, this.wallCollisionRouter);
        this.physics.collide(this.projectileGroup, this.enemyGroup, this.hitCharacter);
        this.physics.collide(this.enemyProjectileGroup, this.player, this.hitPlayer);
        this.physics.collide(this.player, this.wallGroup);
        this.physics.collide(this.enemyGroup, this.wallGroup, this.recalculateWander);
        this.physics.collide(this.player, this.enemyGroup, this.characterCollision);
        this.physics.collide(this.projectileGroup, this.breakableGroup, this.breakableCollision)

        this.enemyGroup.children.entries.forEach((child) => {
            child.update(this.player, this)
        })
        this.player.update()
        const timestamp = new Date()
        this.statusMessages.forEach((message, index) => {
            message.update()
            if ((timestamp - message.timeToLive) > message.initTime) {
                message.delete()
                delete this.statusMessages[index]
            }
        })
        this.breakableGroup.children.entries.forEach((child) => {
            child.update()
        })
    },
    wallCollisionRouter: function (object, wall) {
        object.destroy()
    },
    hitCharacter: function (projectile, character) {
        character.hit(projectile.getDamage())
        projectile.destroy()
    },
    hitPlayer: function (projectile, character) {
        character.hit(1)
        projectile.destroy()
    },
    characterCollision: (player, character) => {
        player.hit(character.getMeleeDamage())
        character.hit(player.getMeleeDamage())
    },
    breakableCollision: (projectile, breakable) => {
        breakable.hit(projectile.getDamage())
        projectile.destroy()
    },
    recalculateWander: (enemy, wall) => {
        enemy.wander()
    },
    gameOver: function () {
        window.timer.setInactive()
        window.removeEventListener('tick', this.gameTickHandler)
        this.backgroundMusic.stop()
        this.scene.start("SceneGameOver", { gameState: this.gameState, gameSettings: this.gameSettings })
    },
})