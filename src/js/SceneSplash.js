import { GameSettings } from './GameSettings.js'
import { GameState } from './GameState.js'

export let SceneSplash = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function () {
        Phaser.Scene.call(this, { "key": "SceneSplash" })
                let resize = () => {
            if (this.sys.game.scene.isActive(this.scene.key)) {
               this.scene.restart()
            }
        }

        window.addEventListener("resize", resize.bind(this), false);
    },
    init: function (gameSettings = null) {
        if (!(gameSettings instanceof GameSettings)) {
            this.gameSettings = new GameSettings()
        } else {
            this.gameSettings = gameSettings
        }

    },
    preload: () => {
    },
    create: function () {
        let menuText = this.add.text(
            this.sys.game.canvas.width / 2,
            this.sys.game.canvas.height / 3,
            "DecaRounds",
            {
                fontSize:64,
                color: "#55FF55",
                fontStyle: "bold"
            }
        ).setOrigin(0.5)

        let subMenuText = this.add.text(
            this.sys.game.canvas.width / 2,
            this.sys.game.canvas.height / 3 + menuText.height,
            "A Ludum Dare 51 Compo Entry",
            {
                fontSize:16,
                color: "#55FF55",
                fontStyle: "bold"
            }
        ).setOrigin(0.5)


        let startButton = this.add.text(
            this.sys.game.canvas.width / 2 - 200,
            this.sys.game.canvas.height / 1.5,
            "Start",
            {
                fontSize: 32,
                color: "#FFFFFF",
                fontStyle: "bold",
                backgroundColor: "#8888ff",
                padding: { x: 10, y: 10 },

            }
        )
            .setOrigin(0.5)
            .setInteractive({ useHandCursor: true })
            .on('pointerdown', () => {
                this.scene.start("SceneStartGame",{gameSettings: this.gameSettings, gameState:  new GameState()})
            })

        let settingsButton = this.add.text(
            this.sys.game.canvas.width / 2 + 200,
            this.sys.game.canvas.height / 1.5,
            "Settings",
            {
                fontSize: 32,
                color: "#FFFFFF",
                fontStyle: "bold",
                backgroundColor: "#ff8888",
                padding: { x: 10, y: 10 },

            }
        )
            .setOrigin(0.5)
            .setInteractive({ useHandCursor: true })
            .on('pointerdown', () => { this.scene.start("SceneGameSettings", this.gameSettings) })

        this.cameras.main.fadeIn(1000, 0, 0, 0)

    },
    startGame: function () {
    },
    update: function () {

    }
})