import { GameSettings } from './GameSettings.js'
import { GameState } from './GameState.js'

export let SceneUI = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function () {
        Phaser.Scene.call(this, { "key": "SceneUI" })
        let resize = () => {
            if (this.sys.game.scene.isActive(this.scene.key)) {
                //this.scene.restart()
            }
        }

        window.addEventListener("resize", resize.bind(this), false);

    },
    init: function (data) {
        if (!(data.gameSettings instanceof GameSettings)) {
            throw 'GameSettings undefined.'
        } else {
            this.gameSettings = data.gameSettings
        }
        if (!(data.gameState instanceof GameState)) {
            throw 'GameState undefined.'
        } else {
            this.gameState = data.gameState
        }

    },
    preload: function () {
        this.load.spritesheet('uiBorder', 'images/ui-border.png', {
            frameWidth: 32,
            frameHeight: 32
        })
        this.load.spritesheet('items', 'images/items.png', {
            frameWidth: 32,
            frameHeight: 32
        })

    },

    create: function () {

        const bottomUIBackground = this.add.tileSprite(0, this.sys.game.canvas.height - 32, this.sys.game.canvas.width, 32, 'uiBorder', 0)
            .setScrollFactor(0)
            .setDepth(11)
            .setScale(2)

        /**
         * Bottom Bar UI
         */
        let uiOffset = 64
        const DayText = this.add.text(uiOffset, this.sys.game.canvas.height - 24, `Current Weapon: ${this.gameState.weaponName}`, {
            fontSize: 24,
            color: "#FFFFFF",
            fontStyle: "bold"
        }
        )
            .setDepth(12)
            .setOrigin(0, 0.5)
            .setScrollFactor(0, 0)
        uiOffset += DayText.width + 380

        /**
         *  Right-aligned Item Collection
         */

        /**
         *  Center-aligned game description
         */
        const PlayerText = this.add.text(uiOffset, this.sys.game.canvas.height - 24, `${this.gameState.playerName}'s DecaRounds Run - Round #${this.gameState.round}`, {
            fontSize: 24,
            color: "#FFFFFF",
            fontStyle: "bold"
        }
        )
            .setDepth(12)
            .setOrigin(0.5)
            .setScrollFactor(0, 0)

            uiOffset += PlayerText.width + 8
            const LeadText = this.add.text(uiOffset, this.sys.game.canvas.height - 24, `Health: ${this.gameState.health}`, {
                fontSize: 24,
                color: "#FFFFFF",
                fontStyle: "bold"
            }
            )
                .setDepth(12)
                .setOrigin(0.5, 0.5)
                .setScrollFactor(0, 0)



    },
    update: function () {
        if (this.gameState.updateUI === true)
        {
            this.gameState.updateUI = false
            this.scene.restart()
        }

    }
})
