export class GameState {
    constructor(playerName) {
        this.playerName = playerName
        this.updateUI = false
        this.maxSpeed = 100
        this.accelerationRate = 10
        this.frictionRate = 1.1
        this.round = 1
        this.updateUI = true
        this.health = 10
        this.meleeDamage = 1
        this.damageMultiplier = 1
        this.invulnerableDuration = 500
        this.fireRate = 1.00
        this.projectileSpeed = 0
    }
    addMaxSpeed(change) {
        this.maxSpeed += change
    }

    addAccelerationRate(change) {
        this.accelerationRate += change
    }

    changeFireRate(change) {
        if (this.fireRate > 0.5) {
            this.fireRate -= change
        }
    }
}