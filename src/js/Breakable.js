import { StatusMessage } from './StatusMessage.js'

export class Breakable extends Phaser.Physics.Arcade.Sprite {
    constructor(scene, position) {

        super(scene, position.x, position.y, 'breakable', Math.floor(Math.random() * 2) + 1)
        this.x = position.x
        this.y = position.y
        this.xMomentum = 0
        this.yMomentum = 0
        this.scene = scene
        this.body = new Phaser.Physics.Arcade.Body(scene.physics.world, this);
        scene.physics.world.enableBody(this, 0)
        this.body.setMass(1000)
        this.scene = scene
        this.health = 10
        scene.add.existing(this);
        this.setCollideWorldBounds(true)

    }

    hit(damage) {
        this.health -= damage
        this.scene.sound.play('enemyHit', {volume: window.soundVolume})
        if (this.health < 1) {
            this.rollReward()
            this.destroy()
        }
    }

    randomRoll() {
        const weights = {
            0: 100,
            1: 100,
            2: 100,
            3: 50,
            4: 50,
            5: 25,
            7: 50
        }
        const weightTotal = Object.values(weights).reduce((partialSum, a) => partialSum + a, 0)

        const random = Math.floor(Math.random() * weightTotal + 1)
        let offset = 0
        for (const [key, value] of Object.entries(weights)) {
            offset += value
            if (offset > random) {
                return key
            }
        }
    }

    rollReward() {
        switch (parseInt(this.randomRoll())) {
            case 0:
                this.scene.gameState.maxSpeed += 10
                this.scene.gameState.accelerationRate += 3
                this.statusNotice('Maximum Speed Increased')
                break;
            case 1:
                this.scene.gameState.health += 1
                this.scene.gameState.updateUI = true
                this.statusNotice('Gained 1 Health')
                break;
            case 2:
                this.scene.gameState.damageMultiplier += 0.10
                this.statusNotice('Damage Multiplier Increased')
                break;
            case 3:
                this.scene.gameState.health += 3
                this.scene.gameState.updateUI = true
                this.statusNotice('Gained 3 Health')
                break;
            case 4:
                this.scene.gameState.meleeDamage++
                this.statusNotice('Melee Damage Increased')
                break;
            case 5:
                this.scene.gameState.invulnerableDuration += 33
                this.statusNotice('Invulnerable Time Increased')
                break;
            case 6:
                this.scene.gameState.changeFireRate(0.01)
                this.statusNotice('Fire Rate Increased')
                break
            case 7:
                this.scene.gameState.projectileSpeed += 5
                this.statusNotice('Projectile Speed Increased')
                break
        }
        
    }

    statusNotice(message) {
        this.scene.statusMessages.push(new StatusMessage(this.scene, this.x, this.y, message))
    }

    update() {
        this.setVelocityX(this.xMomentum)
        this.setVelocityY(this.yMomentum)
    }
}
