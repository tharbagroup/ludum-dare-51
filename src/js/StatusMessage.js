export class StatusMessage {
    constructor(scene, x, y, message) {

        this.initTime = new Date().getTime()
        this.timeToLive = 5000
        this.alpha = 1
        this.message = scene.add.text(
            x,
            y,
            message,
            {
                fontSize: 18,
                color: "#FFFFFF",
                fontStyle: "bold",
                padding: { x: 20, y: 20 },

            }
        )
            .setOrigin(0.5)
    }

    delete() {
        this.message.setVisible(false)
    }

    update() {
        /*
        this.message.position.y++
        this.alpha -= 600/timeToLive
        this.message.setAlpha(this.alpha)
        */
    }
}
