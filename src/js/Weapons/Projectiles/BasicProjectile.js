export class BasicProjectile extends Phaser.Physics.Arcade.Sprite {
    constructor(scene, x, y, xMomentum, yMomentum, angle, texture, textureIndex, damage, damageMultiplier, mass) {

        super(scene, x, y, texture, textureIndex);
        this.xMomentum = xMomentum
        this.yMomentum = yMomentum
        this.angle = angle
        this.texture = texture
        this.body = new Phaser.Physics.Arcade.Body(scene.physics.world, this);
        scene.physics.world.enableBody(this, 0)
        this.body.mass = mass
        this.setVelocityX(xMomentum)
        this.setVelocityY(yMomentum)
        this.frameTime = 100
        this.frameIndex = textureIndex
        this.nextManualFrame = 1
        this.damage = damage
        this.damageMultiplier = damageMultiplier
        scene.add.existing(this);
        this.setCollideWorldBounds(true)
    }

    getDamage() {
        return this.damage * this.damageMultiplier
    }

    calculateNextFrame () {
        const hold = this.frameIndex
        this.frameIndex = this.nextManualFrame
        this.setTexture(this.texture, this.frameIndex)

        if (hold === 1) {
            this.nextManualFrame = 2
        } else if (hold === 2) {
            this.nextManualFrame = 1
        }
        else {
            this.nextManualFrame = 0
        }
    }
}
