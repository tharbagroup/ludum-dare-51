import { BasicProjectile } from './Projectiles/BasicProjectile.js'

export class BaseWeapon {
    constructor(name, damage, shotDelay, maxMomentum, spriteSheet, spriteIndex, sound = 'genericShoot-1') {
        this.name = name
        this.damage = damage
        this.shotDelay = shotDelay
        this.spriteSheet = spriteSheet
        this.spriteIndex = spriteIndex
        this.lastFired = 0
        this.maxMomentum = maxMomentum
        this.sound = sound
        this.damageMultiplier = 1
        this.bulletInertia = 1
        this.mass = 50
    }

    fire(game, targetX, targetY, origin, originMomentumX, originMomentumY) {
        if ((new Date() - this.shotDelay) > this.lastFired) {
            this.playSound(game)
            this.lastFired = new Date()
            const xMomentum = targetX - origin.x
            const yMomentum = targetY - origin.y
            const totalMomentum = Math.abs(xMomentum) + Math.abs(yMomentum)

            origin.projectileGroup.add(new BasicProjectile(
                game,
                origin.x,
                origin.y,
                ((xMomentum / totalMomentum) * this.maxMomentum) + originMomentumX,
                ((yMomentum / totalMomentum) * this.maxMomentum) + originMomentumY,
                Phaser.Math.RadToDeg(Phaser.Math.Angle.Between(origin.x, origin.y, targetX, targetY)),
                this.spriteSheet,
                this.spriteIndex,
                this.damage,
                this.damageMultiplier, 
                this.mass

            )
            )
        }
    }

    fireAtMouse(game, targetX, targetY, origin, originMomentumX, originMomentumY) {
        if ((new Date() - this.shotDelay) > this.lastFired) {
            this.playSound(game)
            this.lastFired = new Date()
            const xMomentum = targetX - (window.innerWidth / 2)
            const yMomentum = targetY - (window.innerHeight / 2)
            const totalMomentum = Math.abs(xMomentum) + Math.abs(yMomentum)

            origin.projectileGroup.add(new BasicProjectile(
                game,
                origin.x,
                origin.y,
                (xMomentum / totalMomentum) * this.maxMomentum + originMomentumX,
                (yMomentum / totalMomentum) * this.maxMomentum + originMomentumY,
                Phaser.Math.RadToDeg(Phaser.Math.Angle.Between(window.innerWidth / 2, window.innerHeight / 2, targetX, targetY)),
                this.spriteSheet,
                this.spriteIndex,
                this.damage,
                this.damageMultiplier, 
                this.mass
            )
            )
        }
    }

    setBulletMass(mass) {
        this.mass = mass
    }

    setDamageMultiplier(damageMultiplier) {
        this.damageMultiplier = damageMultiplier
    }

    playSound(game) {
        if (window.soundVolume === 1) {
        game.sound.play(this.sound, {volume: 0.2})
        }
    }
}