import { BaseWeapon } from "./Weapons/BaseWeapon.js"
export class PlayerWeaponRandomizer {
    constructor() {

    }

    getRandomWeapon(fireRateMultiplier, projectileSpeedEnhance) {

        switch(parseInt(this.randomRoll())) {
            case 0:
                return new BaseWeapon('Basic', Math.ceil(Math.random() * 5), Math.ceil(Math.random() * 3) * 50 + 100 * fireRateMultiplier, 250 + projectileSpeedEnhance, 'projectile', 0, 'genericShoot-1')
            case 1:
                return new BaseWeapon('Super', Math.ceil(Math.random() * 5) + 5, Math.ceil(Math.random() * 3) * 50 + 100 * fireRateMultiplier, 250 + projectileSpeedEnhance, 'projectile', 2, 'genericShoot-1')
            case 2:
                return new BaseWeapon('Super', Math.ceil(Math.random() * 5), Math.ceil(Math.random() * 3) * 50 * fireRateMultiplier, 250 + projectileSpeedEnhance, 'projectile', 2, 'genericShoot-1')  
            case 3:
                return new BaseWeapon('Large', Math.ceil(Math.random() * 5) + 15, Math.ceil(Math.random() * 10) * 50 + 500 * fireRateMultiplier, 200 + projectileSpeedEnhance, 'large-projectile', 0, 'explosion')
            case 4:
                return new BaseWeapon('Laser', Math.ceil(Math.random() * 5), Math.ceil(Math.random() * 3) * 50 * fireRateMultiplier, 400 + projectileSpeedEnhance, 'laser-projectile', 0, 'laserShoot-1')
            case 5:
                return new BaseWeapon('Wave', Math.ceil(Math.random() * 5), Math.ceil(Math.random() * 3) * 50 * fireRateMultiplier, 320 + projectileSpeedEnhance, 'wave-projectile', 0, 'waveShoot-1')
        }

    }

    randomRoll() {
                const weights = {
            0: 100,
            1: 50,
            2: 50,
            3: 25,
            4: 25,
            5: 25
        }
        const weightTotal = Object.values(weights).reduce((partialSum, a) => partialSum + a, 0)

        const random = Math.floor(Math.random() * weightTotal + 1)
        let offset = 0
        for (const [key, value] of Object.entries(weights)) {
            offset += value
            if (offset > random) {
                return key
            }
        }
    }
}