export class Enemy extends Phaser.Physics.Arcade.Sprite {
    constructor(scene, position, weapon) {

        super(scene, position.x, position.y, 'shopkeeper', Math.floor(Math.random() * 4))
        this.x = position.x
        this.y = position.y
        this.scene = scene
        this.xMomentum = 0
        this.yMomentum = 0
        this.maxMomentum = 125
        this.weapon = weapon
        this.body = new Phaser.Physics.Arcade.Body(scene.physics.world, this);
        scene.physics.world.enableBody(this, 0)
        this.scene = scene
        this.frameTime = 100
        this.frameIndex = 0
        this.health = 10 + (Math.random() * this.scene.gameState.round * 2)
        this.nextManualFrame = 1
        this.meleeDamage = 1
        this.followPlayer = false
        scene.add.existing(this);
        this.setCollideWorldBounds(true)
        this.projectileGroup = scene.enemyProjectileGroup
        this.wanderTimer = 0
    }

    hit(damage) {
        this.health -= damage
        this.scene.sound.play('enemyHit', { volume: window.soundVolume })
        if (this.health < 1) {
            this.destroy()
        }
        this.followPlayer = true
    }

    getMeleeDamage() {
        return this.meleeDamage
    }

    fire(game, targetX, targetY) {
        try {
            this.weapon.fire(game, targetX, targetY, this, this.xMomentum, this.yMomentum)
        } catch {}
    }

    lookAtPlayer(player) {
        this.angle = Phaser.Math.RadToDeg(Phaser.Math.Angle.Between(this.x, this.y, player.x, player.y))
    }

    wander() {
        this.wanderTimer = new Date()
        const xTarget = (Math.random() * 2000) - 1000
        const yTarget = (Math.random() * 2000) - 1000
        const totalMomentum = Math.abs(xTarget) + Math.abs(yTarget)
        this.xMomentum = (xTarget / totalMomentum * this.maxMomentum)
        this.yMomentum = (yTarget / totalMomentum * this.maxMomentum)
        this.angle = Phaser.Math.RadToDeg(Phaser.Math.Angle.Between(this.x, this.y, this.x + xTarget, this.y + yTarget))
    }

    update(player, game) {
        if (this.followPlayer === false && Phaser.Math.Distance.Between(player.x, player.y, this.x, this.y) < 300) {
            this.followPlayer = true
        }
        if (this.followPlayer === true) {
            const xMomentum = player.x - this.x
            const yMomentum = player.y - this.y
            const totalMomentum = Math.abs(xMomentum) + Math.abs(yMomentum)
            this.xMomentum = (xMomentum / totalMomentum * this.maxMomentum)
            this.yMomentum = (yMomentum / totalMomentum * this.maxMomentum)
            this.lookAtPlayer(player)
            this.fire(game, player.x, player.y)
        } else {
            if ((this.wanderTimer + 3000) < new Date()) {
                this.wander()
            }
        }
        this.setVelocityX(this.xMomentum)
        this.setVelocityY(this.yMomentum)
    }
}
